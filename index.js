#!/usr/bin/env node
/*
 * @Author: liyonglong
 * @Date: 2019-10-24 21:27:45
 * @Last Modified by: liyonglong
 * @Last Modified time: 2020-01-12 12:42:07
 */

const fs = require('fs')
const path = require('path')

const program = require('commander')

var copyFile = function(srcPath, tarPath, cb) {
  var rs = fs.createReadStream(srcPath)
  rs.on('error', function(err) {
    if (err) {
      console.log('read error', srcPath)
    }
    cb && cb(err)
  })

  var ws = fs.createWriteStream(tarPath)
  ws.on('error', function(err) {
    if (err) {
      console.log('write error', tarPath)
    }
    cb && cb(err)
  })
  ws.on('close', function(ex) {
    cb && cb(ex)
  })

  rs.pipe(ws)
}
function copyFolder(srcDir, tarDir, cb) {
  console.log(`srcDir:${srcDir}`)
  fs.readdir(srcDir, function(err, files) {
    let count = 0
    let checkEnd = function() {
      ++count == files.length && cb && cb()
    }

    if (err) {
      checkEnd()
      return
    }

    files.forEach(function(file) {
      var srcPath = path.join(srcDir, file)
      var tarPath = path.join(tarDir, file)

      fs.stat(srcPath, function(err, stats) {
        if (stats.isDirectory()) {
          console.log('mkdir', tarPath)
          fs.mkdir(tarPath, function(err) {
            if (err) {
              console.log(err)
              return
            }

            copyFolder(srcPath, tarPath, checkEnd)
          })
        } else {
          copyFile(srcPath, tarPath, checkEnd)
        }
      })
    })

    //为空时直接回调
    files.length === 0 && cb && cb()
  })
}

program
  .version('1.1.0')
  .option('-g, --generate', '生成项目')
  .parse(process.argv)

console.log('you ordered a pizza with:')
if (program.generate) {
  const originPath = __dirname
  const name = process.argv[3]
  const currentPath = process.cwd()
  const fullPath = path.join(currentPath, name)
  try {
    fs.mkdirSync(fullPath)
    copyFolder(path.join(originPath, './', 'template/'), fullPath, function(err) {
      if (err) {
        throw err
      }
      console.log(`${name} 创建完成`)
      process.exit(1)
    })
  } catch (error) {
    console.error('项目生成失败')
    throw error
  }
}
